import sqlhelper
import hashlib



def Registration():
    name = input('prosze podac imie:')
    surname = input('prosze podac nazwisko')
    password1 = input('prosze podac haslo')
    password2 = input('prosze podac haslo')
    if(password1 != password2):
        print('hasla sie roznia!')
        menu()
    password = hashlib.sha3_256(password1.encode('utf-8')).hexdigest()
    email = input('prosze podac email')
    sqlhelper.createUser(name, surname, password, email)
    menu()


def login():
    email = input('podaj adres email')
    password = input('podaj haslo')
    hashed_password = hashlib.sha3_256(password.encode('utf-8')).hexdigest()
    user = sqlhelper.logUser(email, hashed_password)
    if len(user) == 0:
        print('nie ma takiego uzytkownika w bazie danych z podanym haslem lub emailem')
    else:
        print(user)
    menu()


def userList():
    users = sqlhelper.usersList()
    for danelogowania in users:
        print(danelogowania[4])
    menu()


def accountDelete():
    email = input('podaj email:')
    password = input('podaj haslo:')
    hashed_password = hashlib.sha3_256(password.encode('utf-8')).hexdigest()
    user_list = sqlhelper.deleteAccount(email, hashed_password)
    if not len(user_list) >0:
        print('Podano nieprawidlowy login lub haslo')
        menu()
    sqlhelper.deleteAccount(email)
    print('usunieto konto')
    menu()

def change_password():
    email = input('podaj email:')
    old_password = input('podaj stare haslo:')
    new_password = input('podaj nowe haslo:')
    repeat_new_password = input('powtorz nowe haslo:')
    old_hashed_password = hashlib.sha3_256(old_password.encode('utf-8')).hexdigest()
    user_list = sqlhelper.find_user_by_email_and_password(email, old_hashed_password)
    if not len(user_list) > 0:
        print('nie ma takiego uzytkownika!')
        menu()

    if new_password != repeat_new_password:
        print('wpisane nowe haslo jest niezgodne')
        menu()

    new_hashed_password = hashlib.sha3_256(new_password.encode('utf-8')).hexdigest()
    sqlhelper.reset_password(email, new_hashed_password)
    print('Zmieniono haslo')
    menu()






def menu():
    print('''
================MENU================
1.Logowanie
2.Rejestracja
3.Lista uzytkownikow bez hasel
4.Resetuj hasło
5.Usun konto
6.Wyjscie    
    ''')
    choise = int(input('Prosze wskazac operacje:'))
    if choise == 1:
        login()
    elif choise == 2:
        Registration()
    elif choise == 3:
        userList()
    elif choise == 4:
        change_password()
    elif choise == 5:
        accountDelete()
    elif choise == 6:
        print('Dziekuje, zamykam')

menu()