import mysql.connector as mysql

def createUser(name, surname, password, email):
    mydb = mysql.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="baza"
    )
    mycursor = mydb.cursor()
    sql = "INSERT INTO danelogowania (name, surname, password, email) VALUES (%s, %s, %s, %s)"
    values = (name, surname, password, email)
    mycursor.execute(sql, values)
    mydb.commit()

def logUser(email, password):
    mydb = mysql.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="baza"
    )
    mycursor = mydb.cursor()
    sql = f"SELECT * FROM danelogowania WHERE EMAIL LIKE '{email}' AND PASSWORD LIKE '{password}'"
    mycursor.execute(sql)
    return mycursor.fetchall()

def usersList():
    mydb = mysql.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="baza"
    )
    mycursor = mydb.cursor()
    mycursor.execute(f"SELECT * FROM danelogowania")
    return mycursor.fetchall()

def reset_password(email, hashed_password):
    mydb = mysql.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="baza"
    )
    mycursor = mydb.cursor()
    sql = f"UPDATE danelogowania SET password = '{hashed_password}' WHERE EMAIL LIKE '{email}' "
    mycursor.execute(sql)
    mydb.commit()

def deleteAccount(email):
    mydb = mysql.connect(
        host="localhost",
        user="root",
        passwd="root",
        database="baza"
    )
    mycursor = mydb.cursor()
    sql = f"DELETE FROM danelogowania WHERE EMAIL LIKE '{email}'"
    mycursor.execute(sql)
    mydb.commit()

def find_user_by_email_and_password(email, password):
    mydb = mysql.connect(
      host="localhost",
      user="root",
      passwd="root",
      database="baza"
    )

    mycursor = mydb.cursor()
    sql = f"SELECT * FROM danelogowania WHERE EMAIL LIKE '{email}' AND PASSWORD LIKE '{password}'"
    mycursor.execute(sql)
    return mycursor.fetchall()
